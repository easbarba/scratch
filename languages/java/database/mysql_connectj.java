///usr/bin/env jbang "$0" "$@" ; exit $?
//DEPS com.mysql:mysql-connector-j:8.3.0
//DEPS com.zaxxer:HikariCP:5.1.0
//DEPS io.github.cdimascio:dotenv-java:3.0.0
//DEPS org.slf4j:slf4j-api:2.0.12
//DEPS org.apache.logging.log4j:log4j-slf4j2-impl:2.23.1

// Scratch is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Scratch is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Scratch. If not, see <https://www.gnu.org/licenses/>.

package dev.easbarba.scracth;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.sql.DataSource;


import com.zaxxer.hikari.HikariDataSource;
import io.github.cdimascio.dotenv.Dotenv;

public class jbang_mysql_connectj {
    public static void main(String[] args) throws Exception {
        var dataSource = createDataSource();

        try (Connection conn = dataSource.getConnection()) {
            var getAllReferees = conn.prepareStatement("select * from referees");
            var rs = getAllReferees.executeQuery();

            while (rs.next()) {
                System.out.printf("Referee name: %s\n", rs.getString("name"));
            }
        }
        catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    private static DataSource createDataSource() {
        Dotenv dotenv = Dotenv.load();

        var ds = new HikariDataSource();
        ds.setJdbcUrl("jdbc:mysql://localhost:" + dotenv.get("SQL_PORT") + "/" + dotenv.get("SQL_DATABASE"));
        ds.setUsername(dotenv.get("SQL_USERNAME"));
        ds.setPassword(dotenv.get("SQL_PASSWORD"));

        return ds;
    }
}
