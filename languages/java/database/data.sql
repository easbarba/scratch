-- Scratch is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Scratch is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Scratch. If not, see <https://www.gnu.org/licenses/>.

SET GLOBAL time_zone = 'America/Sao_Paulo';

--  REFERES
CREATE DATABASE scratch;
USE scratch;

CREATE TABLE referees (
    id VARCHAR(36) PRIMARY KEY NOT NULL,
    name VARCHAR(50) NOT NULL,
    state VARCHAR(19) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

INSERT INTO referees
       (id, name, state)
VALUES
      ('a5047a89-4765-4cc4-972c-8b3f15b6b19c', 'Pericles Bassols Pegado Cortez', 'Rio de Janeiro'),
      ('994d14e7-330d-4a10-af93-6cecc1955aae', 'Raphael Claus',                  'Sao Paulo'),
      ('3dc35734-e5df-405b-bd81-df01f7d129ae', 'Ricardo Marques Ribeiro',        'Minas Gerais'),
      ('6ad63fc2-8cf2-48a6-998a-60d69daa833d', 'Wilton Pereira Sampaio',         'Goias'),
      ('8a8d61a9-21d7-49d1-a4bf-2a479fe9a68f', 'Anderson Daronco',               'Rio Grande do Sul'),
      ('fb6606c6-6fff-464a-834c-bd9851593a27', 'Marcelo Aparecido de Souza',     'Sao Paulo'),
      ('28cfec82-802a-416d-94e3-e0c89d51501e', 'Marcelo de Lima Henrique',       'Rio de Janeiro'),
      ('00ebb8c9-08d6-4fcc-8d49-b5bf53973679', 'Sandro Meira Ricci',             'Minas Gerais'),
      ('d52806ec-329c-4831-9b63-7e9d8f42a914', 'Rafael Traci',                   'Parana'),
      ('d6d4e9e7-2446-4de2-946a-2e0e5ee949ef', 'Dewson Fernando Freitas Silva',  'Parana');
