///usr/bin/env jbang "$0" "$@" ; exit $?
//DEPS org.slf4j:slf4j-api:2.0.12
//DEPS org.apache.logging.log4j:log4j-slf4j2-impl:2.23.1
//DEPS org.eclipse:yasson:3.0.3

// Scratch is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Scratch is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Scratch. If not, see <https://www.gnu.org/licenses/>.

// Eclipse Yasson
// https://projects.eclipse.org/projects/ee4j.yasson

package dev.easbarba.scracth;

import java.util.Date;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;
import jakarta.json.bind.JsonbException;
import jakarta.json.bind.annotation.JsonbDateFormat;
import jakarta.json.bind.annotation.JsonbProperty;

public class Yasson {
    public static void main(String[] args) {
        try {
            JsonbConfig config = new JsonbConfig()
                    .withNullValues(true)
                    .withFormatting(true);
            Jsonb jsonb = JsonbBuilder.create(config);
            var fool = new Fool("Fool", new Date());

            String result = jsonb.toJson(fool);
            System.out.println(result);
        } catch (JsonbException e) {
            System.out.println("Error " + e.getMessage());
            e.printStackTrace();
        }
    }

    public record Fool(
            @JsonbProperty("nome") String name,
            @JsonbDateFormat("dd/MM/yy") Date date) {
    }
}
