#!/usr/bin/guile \
--no-auto-compile -e main -s
!#

;; Scratch is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; Scratch is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with Scratch. If not, see <https://www.gnu.org/licenses/>.

;; -------------- HELPERS

;; transform each char of string to number
;; and return a list of these numbers
(define (string-to-list string)
  (let* ([tolist (string->list string)]
         [todigit (lambda (it)
                    (-  (char->integer it)
                        (char->integer #\0)))]
         [final-list (map todigit tolist)])
    final-list))

;; ----------- MUTABLE VERSION
(define (to-int-base! tolist)
  (when (null? tolist)
    (error "Error: empty string"))

  (letrec ([result 0])
    (apply (lambda (lst)
             (map (lambda (item)
                    (let ([res (+ (* result 10) item)])
                      (set! result res)
                      res))
                  lst))
           tolist)
    result))

(define (toInt! string)
  (to-int-base! (string-to-list string)))

(display (format #f "~a" (toInt! "10742")))


;; ------------ NON-MUTABLE VERSION
(define (to-int-base tolist counter)
  (display counter)
  (if (null? tolist)
      counter
      (+ (* counter 10) (to-int-base (cdr tolist) counter))))

(define (toInt string)
  (to-int-base (string-to-list string) 0))

(display (format #f "~a" (toInt "10678")))
